import json
import os
from statistics import mean, stdev

def load_test_data(data_directory):
    """Loads test data from JSON files in a directory.

    Args:
        data_directory (str): Path to the directory containing JSON files.

    Returns:
        dict: A dictionary where keys are test suite names and values are
              lists of execution times for each build.
    """

    test_data = {}
    for filename in os.listdir(data_directory):
        if filename.endswith('.json'):
            filepath = os.path.join(data_directory, filename)
            with open(filepath, 'r') as f:
                build_data = json.load(f)
                for suite in build_data['test_suites']:
                    suite_name = suite['name']
                    suite_time = suite['total_time']
                    test_data.setdefault(suite_name, []).append(suite_time)
    return test_data

def calculate_baseline_stats(test_data):
    """Calculates baseline statistics (mean, standard deviation) for each test suite.

    Args:
        test_data (dict): Data structure returned by `load_test_data`.

    Returns:
        dict: A dictionary where keys are test suite names and values are
              dictionaries containing 'mean' and 'stdev' keys.
    """

    baseline_stats = {}
    for suite_name, execution_times in test_data.items():
        baseline_stats[suite_name] = {
            'mean': mean(execution_times),
            'stdev': stdev(execution_times)
        }
    return baseline_stats

def detect_anomalies(test_data, baseline_stats, threshold=2.0):
    """Detects anomalies in test suite execution times.

    Args:
        test_data (dict): Data structure returned by `load_test_data`.
        baseline_stats (dict): Baseline statistics returned by `calculate_baseline_stats`.
        threshold (float, optional): Multiplier for standard deviation to determine anomaly threshold. 
                                     Defaults to 2.0.

    Returns:
        dict: A dictionary where keys are test suite names and values are lists of build indices
              where anomalies were detected.
    """

    anomalies = {}
    for suite_name, execution_times in test_data.items():
        stats = baseline_stats[suite_name]
        anomaly_threshold = stats['mean'] + threshold * stats['stdev']
        anomalies[suite_name] = [
             i for i, time in enumerate(execution_times) if time > anomaly_threshold
        ]
    return anomalies

# Example Usage:
DATA_DIR = './data'  # Replace with the path to your data directory
test_data = load_test_data(DATA_DIR)
stats = calculate_baseline_stats(test_data)
anomalies = detect_anomalies(test_data, stats)
print(anomalies)
