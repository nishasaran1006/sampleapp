from django.test import TestCase
from django.core import mail
from django1 import forms

class TestForm(TestCase):
    def test_valid_signup_form_sends_email(self):
        form = forms.UserCreationForm(
            {
                "email": "user@domain.com",
                "password1": "abcabcabc",
                "password2": "abcabcabc",
            }
        )

        self.assertTrue(form.is_valid())

        with self.assertLogs("django1.forms", level="INFO") as cm:
            form.send_mail()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject, "Thank you for joining Appdev"
        )

        self.assertGreaterEqual(len(cm.output), 1)