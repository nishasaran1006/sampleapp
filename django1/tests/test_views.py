from unittest.mock import patch
from django.contrib import auth
from django.test import TestCase
from django.urls import reverse
from django1 import forms
from django1 import models


class TestPage(TestCase):
    def test_home_page_works(self):
        response = self.client.get(reverse("home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "home.html")
        self.assertContains(response, "Appdev")


    def test_items_page_returns_active(self):
        models.Item.objects.create(
            text="This is item 1",
            owner=models.User.objects.create_user(
                "testuser2@email.com", "password"
            ),

        )
        models.Item.objects.create(
            text="This is item 2",
            owner=models.User.objects.create_user(
                "testuser3@email.com", "password"
            ),
        )
        response = self.client.get(
            reverse("home")
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Appdev")
  
        items = models.Item.objects.all().order_by('-updated_at')
   
        self.assertEqual(
            list(response.context["items"]),
            list(items),
        )

    def test_user_signup_page_loads_correctly(self):
        response = self.client.get(reverse("signup"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "signup.html")
        self.assertContains(response, "Appdev")
        self.assertIsInstance(
            response.context["form"], forms.UserCreationForm
        )

    def test_user_signup_page_submission_works(self):
        post_data = {
            "email": "user@domain.com",
            "password1": "abcabcabc",
            "password2": "abcabcabc",
        }
        with patch.object(
            forms.UserCreationForm, "send_mail"
        ) as mock_send:
            response = self.client.post(
                reverse("signup"), post_data
            )

        self.assertEqual(response.status_code, 302)
        self.assertTrue(
            models.User.objects.filter(
                email="user@domain.com"
            ).exists()
        )
        self.assertTrue(
            auth.get_user(self.client).is_authenticated
        )
        mock_send.assert_called_once()

    def test_items_filter_by_owners(self):
        # Creating a user
        test_user = models.User.objects.create_user("testuser@email.com", "password")

        # Creating items belonging to the test user
        item_test_user1 = models.Item.objects.create(
            text="Item 1 belonging to test user",
            owner=test_user,
        )
        item_test_user2 = models.Item.objects.create(
            text="Item 2 belonging to test user",
            owner=test_user,
        )

        # Login as the test user
        self.client.login(username="testuser@email.com", password="password")

        # Request the profile page
        response = self.client.get(reverse("profile"))

        # Check if the response is successful
        self.assertEqual(response.status_code, 200)

        # Sort the items retrieved from the database and from the response context 
        # Because we're filtering the items by updated_at
        expected_items = sorted([item_test_user1, item_test_user2], key=lambda x: x.id)
        actual_items = sorted(response.context["user_items"], key=lambda x: x.id)

        # Check if the items displayed in the response context belong to the test user
        self.assertEqual(actual_items, expected_items)