from django.shortcuts import redirect, render
from django.views.generic.edit import FormView
from django.contrib.auth.views import LogoutView
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.paginator import Paginator
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin 
from django.views.generic.edit import (
    FormView,
    CreateView,
    UpdateView,
    DeleteView,
)

from . import forms
from .models import Item
from django1 import models

class SignupView(FormView): 
    template_name = "signup.html" 
    form_class = forms.UserCreationForm
    
    def get_success_url(self):
        redirect_to = self.request.GET.get("next", "/") 
        return redirect_to
    
    def form_valid(self, form):
        response = super().form_valid(form) 
        form.save()
        email = form.cleaned_data.get("email")
        raw_password = form.cleaned_data.get("password1")
        user = authenticate(email=email, password=raw_password)

        login(self.request, user)
        form.send_mail()

        messages.info(
            self.request, "You signed up successfully."
        )
        return response

class MyLogoutView(LogoutView): 
    template_name = "logout.html"

def home_page(request):
    items = Item.objects.all().order_by('-updated_at')
    paginator = Paginator(items, 10)
    page = request.GET.get('page')
    items = paginator.get_page(page)
    return render(request, "home.html", {'items': items})

@login_required
def new_list(request):
    if request.method == 'POST':
        Item.objects.create(text=request.POST.get("item_text"), owner=request.user)
        return redirect("/")
    else:
        # If the request method is not POST (e.g., GET), redirect to the login page
        return redirect("login") 


def view_list(request,):
    #our_list = List.objects.get(id=list_id)
    items = Item.objects.all()
    return render(request, "list.html", {'items': items})

@login_required
def add_item(request, ):
   # our_list = List.objects.get(id=list_id)
    Item.objects.create(text=request.POST["item_text"],owner=request.user)
    return redirect(f"/lists/")

class ItemDeleteView(LoginRequiredMixin, DeleteView): 
    template_name = "item_confirm_delete.html" 
    model = models.Item
    success_url = reverse_lazy("profile")

    def get_queryset(self):
        return self.model.objects.filter(owner=self.request.user)

@login_required
def profile(request):
    # Filter items by the current user
    user_items = Item.objects.filter(owner=request.user).order_by('-updated_at')
    paginator = Paginator(user_items, 10)
    page = request.GET.get('page')
    user_items = paginator.get_page(page)
    return render(request, 'profile.html', {'user_items': user_items})