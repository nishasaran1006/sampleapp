#!/usr/bin/env bash
DJANGO_SETTINGS_MODULE=appdev.settings \
DJANGO_SECRET_KEY="django-insecure-ao6^0096ia#+j8%c87*0tbbx(jalmx5kliitb=to000tco9u(#" \
DATABASE_NAME=appdev \
DATABASE_USER=postgres \
DATABASE_PASSWORD="postgres" \
EMAIL_HOST="localhost" \
EMAIL_PORT="587" \
EMAIL_HOST_USER="username" \
EMAIL_HOST_PASSWORD="password" \
PIP_REQUIREMENTS=requirements.txt \
docker-compose up --detach --build